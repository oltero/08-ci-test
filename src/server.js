const express = require("express");
const port = 3000;
const app = express();

app.get("/", (req, res) => {
    res.send("Welcome");
});

/** Add two numbers **/
app.get("/multiply", (req, res) => {
    try {
        const multiply = parseInt(req.query.a) * parseInt(req.query.b);
        res.send(multiply.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

if(process.env.NODE_ENV === "test") {
    module.exports = app;
};

if(!module.parent) {
    app.listen(port, () => {
        console.log(`Server running at: localhost:${port}`);
    });
};