const chai = require("chai");
const expect = chai.expect;
const request = require("request")
const app = require("../src/server");
const port = 3000;

let server;

/** Asynchronous request */
const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if(error) {
            reject(error);
        }
        else {
            resolve(response);
        }
    });
});

describe("Test REST API", () => {
    beforeEach("Start server", async () => {
        server = app.listen(port);
    });

    describe("Test multiply", () => {
        it("GET /multiply?a=3&b=4 returns 12", async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/multiply',
                qs: {a: '3', b: '4'}
            };
            await arequest(options).then((res) => {
                console.log({message: res.body});   // res has our data
                expect(res.body).to.equal('12');
            }).catch((res) => {
                console.log({res});
                expect(true).to.equal(false, 'multiply function failed');
            })
        });
    });
    // Check if any failed and close
    afterEach(() => {
        server.close();
    });
});